using Toybox.Application as App;

class Properties {

  function getValue(key) {
    if (App has :Properties) {
      return App.Properties.getValue(key);
    } else {
      return App.getApp().getProperty(key);
    }
  }

  function setValue(key, value) {
    if (App has :Properties) {
      App.Properties.setValue(key, value);
    } else {
      App.getApp().setProperty(key, value);
    }
  }
}