using Toybox.Application;

var version = "1.3.8";
class SmartCadenceApp extends Application.AppBase {

  function initialize() {
    AppBase.initialize();
  }

  // onStart() is called on application start up
  function onStart(state) {
  }

  function onSettingsChanged() {
    getSettings();
  }

  // onStop() is called when your application is exiting
  function onStop(state) {
  }

  // Return the initial view of your application here
  function getInitialView() {
    return [ new SmartCadenceView() ];
  }

}