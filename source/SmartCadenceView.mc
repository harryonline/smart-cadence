/**
* Show cadence or alternative view
* Alternative view triggered when cadence = 0 or every LAPDIST km/mi
*/
using Toybox.WatchUi;
using Toybox.Lang;
using Toybox.System;
using Toybox.Attention;

var CYCLE = 4; // Alternating display cycle
var MINCYCLES = 3; // Number of cycles alternative view lasts
var LAPTIME = 10 * 60 * 1000; // Time that triggers alternative view (10 minutes)

var TONE_ALERT_HI = 4;
var TONE_LAP = 10;

class SmartCadenceView extends WatchUi.SimpleDataField {

  var counter; // count display cycles to ensue minimum time for alternative view
  var nextLap; // next time for triggering alternative view
  var conv; // conversion factor for distamce
  var lastCadence;

  function initialize() {
    SimpleDataField.initialize();
    getSettings();
    label = "Smart Cadence";
    var metric = System.getDeviceSettings().distanceUnits == System.UNIT_METRIC;
    conv = metric ? 1000 : 1609;
    counter = 0;
    nextLap = LAPTIME;
    lastCadence = -1;
  }

  function beep(tone) {
    if (beepSwitch && Attention has :playTone) {
      System.println("Beep");
      Attention.playTone(tone);
    }
  }

  function getDistance(info) {
    return (info.elapsedDistance / conv).format("%.2f");
  }

  function getTime(info) {
    var time = info.timerTime;
    var sec = (time % 60000) / 1000.0;
    var dtime = time / 60000;
    var min = dtime % 60;
    var hour = dtime / 60;
    return Lang.format( "$1$:$2$:$3$", [hour.format("%d"), min.format("%02d"), sec.format("%02d")] );
  }

  function getAlternative(info) {
    var showDistance = info.elapsedDistance != null && counter % CYCLE < CYCLE / 2;
    counter++;
    return showDistance ? getDistance(info) : getTime(info);
  }

  function compute(info) {
    // Before starting the activitiy
    if (info.timerTime == 0) {
      return info.currentCadence == null ? "_ _ _" : info.currentCadence;
    }
    // If no cadence sensor, show alternative
    if (info.currentCadence == null) {
      return getAlternative(info);
    }
    // After LAPTIME ms, show alternative view
    if (info.timerTime != null && info.timerTime >= nextLap) {
      nextLap += LAPTIME;
      beep(TONE_LAP);
      return getAlternative(info);
    }
    // Once alternative started, show at least MINCYCLES cycles
    if (counter > 0 && counter <= CYCLE * MINCYCLES) {
      return getAlternative(info);
    }
    if (info.currentCadence > 0) {
      counter = 0;
      lastCadence = info.currentCadence;
      return info.currentCadence;
    }
    // When no cadence, show alternative view
    if (counter == 0) {
      // Beep when triggered
      beep(TONE_ALERT_HI);
    }
    return getAlternative(info);
  }
}