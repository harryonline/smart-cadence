const groen = '#4caf50';
const rood = '#f44336';
const wit = '#ffffff';

const size = '480'
const wielKleur = groen;
const wielDikte = 4;
const trapperKleur = rood;
const trapperDikte = 4;
const asDikte = 4;

const svg = `<svg height='${size}' width='${size}' fill="${wielKleur}" 
  xmlns="http://www.w3.org/2000/svg" 
  xmlns:xlink="http://www.w3.org/1999/xlink" version="1.2" baseProfile="tiny" x="0px" y="0px" viewBox="0 0 128 128" xml:space="preserve">
  <ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -47.1543 113.8406)" fill="none" stroke="${trapperKleur}" stroke-width="${asDikte}" stroke-linejoin="round" stroke-miterlimit="10" cx="113.8" cy="113.8" rx="4" ry="4"></ellipse>
  <path fill="none" stroke="${trapperKleur}" stroke-width="${trapperDikte}" stroke-linejoin="round" stroke-miterlimit="10" d="M121.6,106.1  c4.3,4.3,4.3,11.3,0,15.6s-11.3,4.3-15.6,0"></path>
  <path fill="none" stroke="${wielKleur}" stroke-width="${wielDikte}" stroke-linejoin="round" stroke-miterlimit="10" d="M45.8,17.8  c-6.4,1.9-12.4,5.4-17.5,10.5c-5.1,5.1-8.6,11.1-10.5,17.5c0.3,3,1.6,10.6,5.5,10.1c4.9-0.7,23.3-2.1,26.9-5.7s4.9-21.9,5.7-26.9  C56.4,19.4,48.8,18.1,45.8,17.8z"></path>
  <path fill="none" stroke="${wielKleur}" stroke-width="${wielDikte}" stroke-linejoin="round" stroke-miterlimit="10" d="M94.7,78.5  c1.5-2.6,2.6-5.4,3.5-8.3c-0.3-3-1.6-10.6-5.5-10.1c-2.6,0.4-8.7,0.9-14.6,1.9"></path>
  <path fill="none" stroke="${wielKleur}" stroke-width="${wielDikte}" stroke-linejoin="round" stroke-miterlimit="10" d="M62,77.9  c-1,5.9-1.5,12.1-1.9,14.7c-0.6,4,7,5.2,10.1,5.5c2.9-0.9,5.7-2.1,8.4-3.6"></path>
  <path fill="none" stroke="${wielKleur}" stroke-width="${wielDikte}" stroke-linejoin="round" stroke-miterlimit="10" d="M87.5,27.8  c-5.1-5.1-11.1-8.6-17.5-10.5V42c0,2.2,1.8,4,4,4h24.2C96.3,39.3,92.7,33,87.5,27.8z"></path>
  <path fill="none" stroke="${wielKleur}" stroke-width="${wielDikte}" stroke-linejoin="round" stroke-miterlimit="10" d="M17.3,70  c1.9,6.4,5.4,12.4,10.5,17.5C33,92.7,39.3,96.3,46,98.2L46,74c0-2.2-1.8-4-4-4H17.3z"></path>
  <path fill="none" stroke="${wielKleur}" stroke-width="${wielDikte}" stroke-linejoin="round" stroke-miterlimit="10" d="M88.8,104.8  c-21.7,14.4-51.3,12-70.4-7.2c-21.9-21.9-21.9-57.3,0-79.2s57.3-21.9,79.2,0c19.1,19.1,21.5,48.6,7.2,70.3"></path>
  <line fill="none" stroke="${trapperKleur}" stroke-width="${trapperDikte}" stroke-linejoin="round" stroke-miterlimit="10" x1="50.2" y1="65.8" x2="106.1" y2="121.6"></line>
  <line fill="none" stroke="${trapperKleur}" stroke-width="${trapperDikte}" stroke-linejoin="round" stroke-miterlimit="10" x1="65.8" y1="50.2" x2="121.6" y2="106.1"></line>
</svg>`;

console.log(svg);
